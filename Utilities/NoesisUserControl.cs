﻿using CV;

using Noesis;

namespace NoesisExtensions.Utilities
{
    public abstract class NoesisUserControl : UserControl
    {
        public NoesisUserControl()
        {
            this.Initialized += new WeakEventHandler<object, EventArgs>(OnInitialized).Handler;
            this.Loaded += new WeakEventHandler<object, RoutedEventArgs>(OnLoaded).Handler;
            this.Unloaded += new WeakEventHandler<object, RoutedEventArgs>(OnUnloaded).Handler;
        }

        virtual protected void OnInitialized(object sender, EventArgs args){}

        virtual protected void OnLoaded(object sender, RoutedEventArgs args) { }

        virtual protected void OnUnloaded(object sender, RoutedEventArgs args) { }
    }
}