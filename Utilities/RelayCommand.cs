﻿using System;
using System.ComponentModel;
using System.Windows.Input;

using Noesis;

namespace NoesisExtensions.Utilities
{

    public class RelayCommand : ICommand {

        public event EventHandler CanExecuteChanged;

        Action<object> _executeAction;

        Predicate<object> _canExecuteAction;

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {         
            _executeAction = execute;         
            _canExecuteAction = canExecute;         
        }


        public bool CanExecute(object parameter)
		{         
            return _canExecuteAction( parameter );         
        }

        public void Execute(object parameter)
		{         
            if (_executeAction != null)
                _executeAction( parameter );         
        }

        public void UpdateCanExecute()
		{         
            if (CanExecuteChanged != null)
                CanExecuteChanged ( this, new System.EventArgs() );         
        }

        private void updateCanExecute( object sender, PropertyChangedEventArgs args )
		{
            UpdateCanExecute ();
        }

    }
        
}
