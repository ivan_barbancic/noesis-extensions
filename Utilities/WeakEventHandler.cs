﻿using System;
using System.Reflection;

namespace NoesisExtensions.Utilities
{
    public sealed class WeakEventHandler<TEventArgs1, TEventArgs2>
    {
        private readonly WeakReference _targetReference;
        private readonly MethodInfo _method;

        public WeakEventHandler(Action<TEventArgs1, TEventArgs2> callback)
        {
            _method = callback.Method;
            _targetReference = new WeakReference(callback.Target, true);
        }

        public void Handler(TEventArgs1 e1, TEventArgs2 e2)
        {
            var target = _targetReference.Target;
            if (target != null)
            {
                var callback = (Action<TEventArgs1, TEventArgs2>)Delegate.CreateDelegate(typeof(Action<TEventArgs1, TEventArgs2>), target, _method, true);
                if (callback != null)
                {
                    callback(e1, e2);
                }
            }
        }
    }
}
