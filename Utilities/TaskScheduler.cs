﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace NoesisExtensions.Utilities
{   
	public class TaskScheuler : MonoBehaviour
    {      
        public static event Action OnUpdate;

		static TaskScheuler _instance;

		Queue<Action> _executionQueue;

		static TaskScheuler()
		{
			GameObject go = new GameObject();
			_instance = go.AddComponent<TaskScheuler>();
		}

        // Use this for initialization
        void Awake()
        {       
            _instance = this;
			_executionQueue = new Queue<Action>();
        }

        // Update is called once per frame
        void Update()
        {         
            if (OnUpdate != null)
                OnUpdate();

			if (_executionQueue == null)
                return;

            if (_executionQueue.Count > 0)
                _executionQueue.Dequeue().Invoke();         
        }

		public static void RunOnMainThread(Action task)
        {         
            if (_instance == null)
                throw new InvalidOperationException(
                    "The scene does not containe a MainThreadTaskScheduler" +
                    ", add the component to the scene");

            _instance._executionQueue.Enqueue(task);

        }
    }
}
