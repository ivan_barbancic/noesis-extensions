﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;
using System.Collections;

namespace NoesisExtensions.Utilities
{
	public static class UnityHelper 
	{
        public static void StartsCoroutine(IEnumerator routine)
        {
            UnityHelperGameObject.Instance.StartCoroutine(routine);    
        }

		public static void LoadTextureAsync( Texture2D texture, string url, Action onComplete, Action<string> onFail)
		{
			UnityHelperGameObject.Instance.LoadTextureAsync (texture, url, onComplete, onFail);
		}

		public static void DownloadTextureAsync(string url, string destinationPath, Action onComplete)
        {
			UnityHelperGameObject.Instance.DownloadTextureAsync(url, destinationPath, onComplete);
        }

        public static byte[] ConvertToJpgImage(byte[] image)
        {
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(image);
            UnityHelper.ApplyWhiteOnTransparentArea(texture);
            texture.Apply();
            return texture.EncodeToJPG();
        }

        public static Texture2D GetEmptyTexture()
        {
            if (SystemInfo.SupportsTextureFormat(TextureFormat.RGBAHalf))
                return new Texture2D(1, 1, TextureFormat.RGBAHalf, false);
            else if (SystemInfo.SupportsTextureFormat(TextureFormat.ARGB32))
                return new Texture2D(1, 1, TextureFormat.ARGB32, false);
            else
                return new Texture2D(1, 1);
        }

        public static void ApplyWhiteOnTransparentArea(Texture2D texture)
        {
            for (int i = 0; i < texture.width; i++)
            {
                for (int j = 0; j < texture.height; j++)
                {
                    var originalColor = texture.GetPixel(i, j);
                    if (originalColor.a < 1f)
                    {
                        var newColor = UnityEngine.Color.Lerp(UnityEngine.Color.white, originalColor, originalColor.a);
                        newColor.a = 1;
                        texture.SetPixel(i, j, newColor);
                    }
                }
            }
        }

        public static void ApplyPremultipliedAlpha(Texture2D texture)
        {
            UnityEngine.Debug.Log("Aplying premultipled alpha");
            int width = texture.width;
            int height = texture.height;
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    Color color = texture.GetPixel(x, y);
                    texture.SetPixel(x, y, Premultiply(color));
                }
            }

            texture.Apply();
        }

        private static Color Premultiply(Color color)
        {
            float a = color.a;
            return new Color(color.r * a, color.g * a, color.b * a, a);
        }
	}

	public class UnityHelperGameObject : Singleton<UnityHelperGameObject> 
	{        
		public void LoadTextureAsync( Texture2D texture, string url, Action onComplete, Action<string> onFail = null)
		{
			var loadTextureArgumetns = new LoadTextueArgument {
				Texture = texture,
				Url = url,
				OnComplete = onComplete
			};

			StartCoroutine ("LoadTexture", loadTextureArgumetns);
		}

		IEnumerator LoadTexture(LoadTextueArgument loadTextueArgument)
		{
			WWW www = new WWW(loadTextueArgument.Url);
			yield return www;
			if (www.error == null)
			{
				if (loadTextueArgument.Texture != null)
				{
					ImageConversion.LoadImage(loadTextueArgument.Texture, www.bytes, true);
				}
				loadTextueArgument.OnComplete();
			}else
			{
				if (loadTextueArgument.OnFail != null)
					loadTextueArgument.OnFail(www.error);
			}         
		}

		public void DownloadTextureAsync(string url, string destinationPath, Action onComplete)
        {
			var downloadTextureArgumetns = new DownloadTextureArgument
            {
				DestinationPath = destinationPath,
                Url = url,
                OnComplete = onComplete
            };

			StartCoroutine("DownloadTexture", downloadTextureArgumetns);
        }

		IEnumerator DownloadTexture(DownloadTextureArgument downloadTextureArgument)
        {
			WWW www = new WWW(downloadTextureArgument.Url);
            yield return www;
			File.WriteAllBytes(downloadTextureArgument.DestinationPath, www.bytes);
			downloadTextureArgument.OnComplete();
        }
	}

	public class LoadTextueArgument
	{
		public Texture2D Texture { get; set; }
		public string Url {get; set;}
		public Action OnComplete { get; set;}
		public Action<string> OnFail { get; set; }
	}

	public class DownloadTextureArgument
    {
		public string Url { get; set; }
		public string DestinationPath { get; set; }
		public Action OnComplete { get; set; }
    }
}
