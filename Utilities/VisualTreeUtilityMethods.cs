﻿using System;
using System.Collections;

using Noesis;

namespace NoesisExtensions.Utilities
{   
    public static class VisualTreeUtilityMethods
    {      
        public static Visual GetDescendantByType(Visual element, Type type)
        {
            return GetDescendantByType(element, type, false);
        }

        public static Visual GetDescendantByType(Visual element, Type type, bool skipFirstElement)
        {
            if (element == null)
            {
                return null;
            }
            if (element.GetType() == type && !skipFirstElement)
            {
                return element;
            }
            Visual foundElement = null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
            {
                Visual visual = VisualTreeHelper.GetChild(element, i) as Visual;
                foundElement = GetDescendantByType(visual, type);
                if (foundElement != null)
                {
                    break;
                }
            }
            return foundElement;

        }

        public static T GetParentOfType<T>(Visual start, Visual end) where T : Visual
        {
            T result = null;
            Visual iterator = start;
            while (true)
            {
                if (iterator == null || iterator == end)
                    break;

                if (iterator is T)
                {
                    result = iterator as T;
                    break;
                }

                iterator = VisualTreeHelper.GetParent(iterator);
            }
            return result;
        }

        public static T GetParentByName<T>(Visual start, Visual end, string name) where T : FrameworkElement
        {
            T result = null;
            Visual iterator = start;
            while (true)
            {
                if (iterator == null || iterator == end)
                    break;
                if (iterator is T && (iterator as T).Name == name)
                {
                    result = iterator as T;
                    break;
                }

                iterator = VisualTreeHelper.GetParent(iterator);
            }
            return result;
        }
    }   
}
