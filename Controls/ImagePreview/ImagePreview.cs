﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using Noesis;
using UnityEngine;

using NoesisExtensions.Utilities;

namespace NoesisExtensions.Controls
{   
    public partial class ImagePreview : Image
    {  
        public static DependencyProperty ImageUrlProperty = DependencyProperty.Register("ImageUrl", typeof(string), typeof(ImagePreview),
            new FrameworkPropertyMetadata("", new PropertyChangedCallback(imageUrlChanged)));

		public static DependencyProperty ImageStatusProperty = DependencyProperty.Register("ImageStatus", typeof(ImageStatus), typeof(ImagePreview),
            new FrameworkPropertyMetadata(null));

        public string ImageUrl
        {
            get { return (string)GetValue(ImageUrlProperty); }
            set { SetValue(ImageUrlProperty, value); }
        }
        
		public ImageStatus ImageStatus
        {
			get { return (ImageStatus)GetValue(ImageStatusProperty); }
			private set { SetValue(ImageStatusProperty, value); }
        }

        Texture2D _texture;

        public ImagePreview()
        {
			this.Loaded += OnLoaded;
            this.Unloaded += OnUnloaded;
        }

        protected void OnLoaded(object sender, Noesis.RoutedEventArgs args)
        {         
            _texture = UnityHelper.GetEmptyTexture();
			ImageStatus = ImageStatus.UrlNotSet;

            if (!string.IsNullOrEmpty(ImageUrl))
            {
			    loadImage(ImageUrl);
            }
        }

        protected void OnUnloaded(object sender, Noesis.RoutedEventArgs args)
        {
            if (!(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor))
                UnityEngine.Object.Destroy(_texture);
            _texture = null;
        }

        private void loadImage(string imagePath)
        {
			TaskScheuler.RunOnMainThread(() => 
			{     
                if (string.IsNullOrEmpty(imagePath))
                {
					this.Source = null;
					ImageStatus = ImageStatus.Loaded;
                }
                else
                {
					ImageStatus = ImageStatus.Loading;
                    UnityHelper.LoadTextureAsync(_texture, (new Uri(imagePath)).AbsoluteUri, () => {
                        if (_texture != null)
                        {
                            TextureSource source = new TextureSource(_texture);
                            this.Source = source;
							ImageStatus = ImageStatus.Loaded;
                        }
					}, (error)=>{});
                }
            });         
        }

        private static void imageUrlChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            ImagePreview control = sender as ImagePreview;
            string newUrl = args.NewValue.ToString();
                     
            if (string.IsNullOrEmpty(newUrl))
            {
                control.Source = null;
			}else
			{
				control.loadImage(newUrl); 
			}      
        }      
    }

}
