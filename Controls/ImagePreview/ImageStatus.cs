﻿namespace NoesisExtensions.Controls
{      
    public enum ImageStatus
    {
        UrlNotSet,
        Loading,
        Loaded
    }
}