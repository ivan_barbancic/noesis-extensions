﻿using System.Windows.Input;

using Noesis;

using UnityEngine;
using UnityEngine.Video;

using NoesisExtensions.Utilities;

namespace NoesisExtensions.Controls
{
	public partial class VideoPlayer : Control
	{      
		#region Dependency properties

		public static DependencyProperty UrlProperty = DependencyProperty.Register("Url", typeof(string), typeof(VideoPlayer),
			new FrameworkPropertyMetadata("", new PropertyChangedCallback(urlChanged)));

		public static DependencyProperty PlayCommandProperty = DependencyProperty.Register("PlayCommand", typeof(ICommand), typeof(VideoPlayer));

		public static DependencyProperty PauseCommandProperty = DependencyProperty.Register("PauseCommand", typeof(ICommand), typeof(VideoPlayer));

		public static DependencyProperty RewindCommandProperty = DependencyProperty.Register("RewindCommand", typeof(ICommand), typeof(VideoPlayer));

		public static DependencyProperty CurrentTimeProperty = DependencyProperty.Register("CurrentTime", typeof(double), typeof(VideoPlayer),
			new FrameworkPropertyMetadata(0f));

		public static DependencyProperty VideoLengthProperty = DependencyProperty.Register("VideoLength", typeof(double), typeof(VideoPlayer),
			new FrameworkPropertyMetadata(0f));

		public static DependencyProperty VolumeProperty = DependencyProperty.Register("Volume", typeof(double), typeof(VideoPlayer),
			new FrameworkPropertyMetadata(0.0, new PropertyChangedCallback(volumeChanged)));

		public static DependencyProperty ErrorMessageProperty = DependencyProperty.Register("ErrorMessage", typeof(string), typeof(VideoPlayer),
			new FrameworkPropertyMetadata(null));

		public static DependencyProperty VideoStatusProperty = DependencyProperty.Register("VideoStatus", typeof(VideoStatus), typeof(VideoPlayer),
            new FrameworkPropertyMetadata(null));


		public string Url
		{
			get { return (string)GetValue(UrlProperty); }
			set { SetValue(UrlProperty, value); }
		}

		public ICommand PlayCommand
		{
			get { return (ICommand)GetValue(PlayCommandProperty); }
			set { SetValue(PlayCommandProperty, value); }
		}

		public ICommand PauseCommand
		{
			get { return (ICommand)GetValue(PauseCommandProperty); }
			set { SetValue(PauseCommandProperty, value); }
		}

		public ICommand RewindCommand
		{
			get { return (ICommand)GetValue(RewindCommandProperty); }
			set { SetValue(RewindCommandProperty, value); }
		}

		public double CurrentTime
		{
			get { return (double)GetValue(CurrentTimeProperty); }
			set { SetValue(CurrentTimeProperty, value); }
		}

		public double VideoLength
		{
			get { return (double)GetValue(VideoLengthProperty); }
			set { SetValue(VideoLengthProperty, value); }
		}

		public double Volume
		{
			get { return (double)GetValue(VolumeProperty); }
			set { SetValue(VolumeProperty, value); }
		}

		public string ErrorMessage
		{
			get { return (string)GetValue(ErrorMessageProperty); }
			set { SetValue(ErrorMessageProperty, value); }
		}

		public VideoStatus VideoStatus
        {
			get { return (VideoStatus)GetValue(VideoStatusProperty); }
			private set { SetValue(VideoStatusProperty, value); }
        }

		#endregion

		#region private variables

		UnityEngine.Video.VideoPlayer _videoPlayer;

		Image _image;
		Slider _videoSlider;
		FrameworkElement _commandContainer;

		bool _sliderManipulationInProgress = false;
		bool _isCommandContainerVisible = true;
		bool _endReached = false;

		#endregion

		#region Initialization

		static VideoPlayer()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(VideoPlayer),
				new FrameworkPropertyMetadata(typeof(VideoPlayer)));
		}

		public VideoPlayer() //: base()
		{
			this.Loaded += OnLoaded;
			this.Unloaded += OnUnloaded;
		}

		protected void OnLoaded(object sender, Noesis.RoutedEventArgs args)
		{
			_commandContainer = GetTemplateChild("PART_CommandContainer") as FrameworkElement;
			_image = GetTemplateChild("PART_Image") as Image;
			_videoSlider = GetTemplateChild("PART_VideoSlider") as Slider;

			_videoPlayer = createNewVideoPlayer();

			_videoSlider.ValueChanged += videoSlider_ValueChanged;

			_videoSlider.MouseDown += videoSlider_MouseDown;
			_videoSlider.MouseUp += videoSlider_MouseUp;

			_videoSlider.TouchDown += videoSlider_TouchDown;
			_videoSlider.TouchUp += videoSlider_TouchUp;

			_videoPlayer.prepareCompleted += onPrepareCompleted;
			_videoPlayer.loopPointReached += onPlayerEndReached;
			_videoPlayer.errorReceived += onErrorRecieved;

			TaskScheuler.OnUpdate += onUpdate;

			VideoStatus = VideoStatus.UrlNotSet;

			if (!string.IsNullOrEmpty(Url))
				setupUrl(Url);
			setupVolume(Volume);

			PlayCommand = new RelayCommand(execute_Play, canExecute_Play);
			PauseCommand = new RelayCommand(execute_Pause, canExecute_Pause);
			RewindCommand = new RelayCommand(execute_Rewind, canExecute_Rewind);
		}

		protected void OnUnloaded(object sender, Noesis.RoutedEventArgs args)
		{
			UnityEngine.Debug.Log("On unloaded");
			_videoSlider.ValueChanged -= videoSlider_ValueChanged;

			_videoSlider.MouseDown -= videoSlider_MouseDown;
			_videoSlider.MouseUp -= videoSlider_MouseUp;

			_videoSlider.TouchDown -= videoSlider_TouchDown;
			_videoSlider.TouchUp -= videoSlider_TouchUp;

			_videoPlayer.prepareCompleted -= onPrepareCompleted;
			_videoPlayer.loopPointReached -= onPlayerEndReached;
			_videoPlayer.errorReceived -= onErrorRecieved;

			TaskScheuler.OnUpdate -= onUpdate;
			releaseVideoPlayer();
		}

		#endregion

		#region Dependency property changed event handlers

		private static void urlChanged(object sender, DependencyPropertyChangedEventArgs args)
		{
			var videoPlayer = sender as VideoPlayer;
			var url = args.NewValue as string;
			if (!string.IsNullOrEmpty(url) && videoPlayer.IsLoaded)
				videoPlayer.setupUrl(url);
			videoPlayer.updateCommands();
		}

		private static void volumeChanged(object sender, DependencyPropertyChangedEventArgs args)
		{
			var videoPlayer = sender as VideoPlayer;
			if (videoPlayer.IsLoaded)
				videoPlayer.setupVolume((double)args.NewValue);
		}

		private void setupUrl(string url)
		{
			_image.Source = null;
			_videoPlayer.url = url;

			_videoPlayer.time = 0;
			_videoSlider.Value = 0;
			CurrentTime = 0.0;
			_endReached = false;
			_videoPlayer.SetDirectAudioVolume(0, 0.5f);         
			_videoPlayer.Prepare();
			VideoStatus = VideoStatus.Preparing;
		}

		private void setupVolume(double volume)
		{
			_videoPlayer.GetTargetAudioSource(0).volume = (float)volume / 100f;
		}

		#endregion

		#region Command methods

		private void execute_Play(object args)
		{
			_videoPlayer.Play();
			updateCommands();
		}

		private bool canExecute_Play(object args)
		{
			if (_videoPlayer == null)
				return false;
			else
				return !_videoPlayer.isPlaying && !_endReached && _videoPlayer.isPrepared;
		}

		private void execute_Pause(object args)
		{
			_videoPlayer.Pause();
			updateCommands();
		}

		private bool canExecute_Pause(object args)
		{
			if (_videoPlayer == null)
                return false;
            else			
			    return _videoPlayer.isPlaying && !_endReached;
		}

		private void execute_Rewind(object args)
		{
			_videoPlayer.time = 0;
			_videoSlider.Value = 0f;
			_endReached = false;
			_videoPlayer.Play();
			updateCommands();
		}

		private bool canExecute_Rewind(object args)
		{
			return _endReached;
		}

		#endregion

		#region GUI Event Handlers

		private void commandContainer_MouseMove(object sender, MouseEventArgs args)
		{
			if (_isCommandContainerVisible)
			{
				_commandContainer.Visibility = Visibility.Visible;
				_isCommandContainerVisible = true;
			}
		}

		private void videoSlider_ValueChanged(float oldValue, float newValue)
		{
			CurrentTime = (double)(newValue * (float)VideoLength);

			if (_sliderManipulationInProgress)
			{
				if (_endReached)
				{
					_endReached = false;
					updateCommands();
				}
				else
				{
					_videoPlayer.time = (double)(newValue * VideoLength);
				}
			}
		}

		public void videoSlider_MouseDown(object sender, MouseEventArgs args)
		{
			_sliderManipulationInProgress = true;
			_videoPlayer.Pause();
		}

		public void videoSlider_MouseUp(object sender, MouseEventArgs args)
		{
			if (_sliderManipulationInProgress)
			{
				_sliderManipulationInProgress = false;
				_videoPlayer.time = VideoLength * _videoSlider.Value;
				_videoPlayer.Play();
				updateCommands();
			}
		}

		public void videoSlider_TouchDown(object sender, TouchEventArgs args)
		{
			_sliderManipulationInProgress = true;
			_videoPlayer.Pause();
		}

		public void videoSlider_TouchUp(object sender, TouchEventArgs args)
		{
			if (_sliderManipulationInProgress)
			{
				_sliderManipulationInProgress = false;
				_videoPlayer.time = VideoLength * _videoSlider.Value;
				_videoPlayer.Play();
				updateCommands();
			}
		}

		#endregion

		#region Player Event handler

		void onPlayerEndReached(UnityEngine.Video.VideoPlayer source)
		{
			_endReached = true;
			_videoPlayer.Stop();
			_image.Source = null;
			_videoSlider.Value = 1f;
			updateCommands();
		}

		void onPrepareCompleted(UnityEngine.Video.VideoPlayer source)
		{
			VideoLength = _videoPlayer.frameCount / _videoPlayer.frameRate;
			_image.Source = new TextureSource(source.texture);
			VideoStatus = VideoStatus.Prepared;
			updateCommands();
		}

		void onErrorRecieved(UnityEngine.Video.VideoPlayer source, string message)
		{
			ErrorMessage = message;
		}
        
		#endregion

		#region Video player caching

		static ushort audioTrackNum = 0;

		static ushort audioTrackCount = 1;
              
		public void releaseVideoPlayer()
		{
			GameObject.Destroy(_videoPlayer.gameObject);
			_videoPlayer = null;
		}

		private static UnityEngine.Video.VideoPlayer createNewVideoPlayer()
		{
			GameObject go = new GameObject();
			var video = go.AddComponent<UnityEngine.Video.VideoPlayer>();
			var audio = go.AddComponent<AudioSource>();

			// Set VideoPlayer Output Mode & Movie Source
			video.audioOutputMode = VideoAudioOutputMode.AudioSource;
			video.source = VideoSource.Url;

			// Disable Video & Audio Players play onAwake
			video.playOnAwake = false;
			audio.playOnAwake = false;

			// Set full Movie Path
			video.controlledAudioTrackCount = audioTrackCount;

			// Audio Track Enable
			for (int i = 0; i <= audioTrackCount - 1; i++)
			{
				video.EnableAudioTrack((ushort)i, false);
			}
			video.EnableAudioTrack(audioTrackNum, true);
			video.SetTargetAudioSource(audioTrackNum, audio);

			video.renderMode = VideoRenderMode.RenderTexture;

			return video;
		}

		#endregion

		#region Time update

		public void onUpdate()
		{
			if (_sliderManipulationInProgress)
				return;

			if (_videoPlayer.isPlaying)
			{
				_videoSlider.Value = (float)(CurrentTime / VideoLength);
				CurrentTime = _videoPlayer.time;
			}
		}

		#endregion

		#region utilities

        private void updateCommands()
		{
			(PlayCommand as RelayCommand).UpdateCanExecute();
			(PauseCommand as RelayCommand).UpdateCanExecute();
            (RewindCommand as RelayCommand).UpdateCanExecute();
		}

		#endregion
	}

}
