﻿namespace NoesisExtensions.Controls
{      
    public enum VideoStatus
    {
    	UrlNotSet,
    	Preparing,
    	Prepared
    }
}