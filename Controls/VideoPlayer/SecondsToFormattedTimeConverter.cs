﻿using System;
using System.Collections;

using Noesis;

namespace NoesisExtensions.Controls
{
    public class SecondsToFormattedTimeConverter : IValueConverter
    {      
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
			var seconds = (double)value;
			var time = TimeSpan.FromSeconds(seconds);         
            return string.Format("{0:D2}:{1:D2}", time.Minutes, time.Seconds);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
			throw new NotImplementedException();      
        }
    }

}
