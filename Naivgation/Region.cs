﻿using System;
using System.Linq;
using System.Collections.Generic;

using Noesis;

using NoesisExtensions.Utilities;

namespace NoesisExtensions.Navigation
{
	public class Region : NoesisUserControl
	{      
		private List<RegsiteredView> _views;

		Action<string, object[]> _lastRequestNavigate;

		public Region()
		{
			_views = new List<RegsiteredView>();
		}

		public void Register(string viewName, Func<UserControl> resolver)
		{
			_views.Add(new RegsiteredView{ViewName = viewName, Resolve = resolver});
		}

		public void Unregister(string viewName)
        {
			throw new NotImplementedException();
        }

		public void NavigateTo(Action<string,object[]> requestNavigate, string viewName, params object[] parameters)
		{
			_lastRequestNavigate = requestNavigate;

			if(hasRegitseredType(viewName))
			{
				navigateToView(viewName, parameters);
			}else{
				throw new NotImplementedException();
			}
		}

		private void requestNavigate(string viewName, object[] parameters)
		{
			if(hasRegitseredType(viewName))
			{
				navigateToView(viewName, parameters);
			}else{
				_lastRequestNavigate(viewName, parameters);
			}
		}

		private void requestNavigateModal(string viewName, object[] parameters, Action<object[]> completed)
        {
            if (hasRegitseredType(viewName))
            {
                navigateToView(viewName, parameters);
            }
            else
            {
                _lastRequestNavigate(viewName, parameters);
            }
        }

        private void navigateToView(string viewName, object[] parameters)
		{
			var view = resolveView(viewName);
            var currentView = this.Content as INavigationAware;
            if (view != null)
            {
                currentView.RequestNavigateFrom(() =>
                {
                    currentView.NavigateFrom(() =>
                    {
                        this.Content = view;
						view.NavigateTo(new NavigateRequest(requestNavigate, requestNavigateModal), parameters);  
                    });
                });
            }
		}

		private void navigateToViewModal(string viewName, object[] parameters, Action<object[]> complete)
        {
			var view = resolveView(viewName) as IModalNavigationAware;
			var currentView = this.Content as IModalNavigationAware;
            if (view != null)
            {
				view.NavigateToModal((object[] parameters2) => {
					view.RequestNavigateFrom(() => {
						view.NavigateFrom(() => {
							complete(parameters2);
						});
					});
				}, parameters);
            }
        }

        private bool hasRegitseredType(string viewName)
		{
			return _views.Any(x => x.ViewName == viewName);
		}

        private INavigationAware resolveView(string viewName)
		{
			var registeredView = _views.First(x => x.ViewName == viewName);
		    return registeredView.Resolve() as INavigationAware;
		}

		private class RegsiteredView
		{
			public string ViewName;
			public Func<UserControl> Resolve;
		}
	}
}
