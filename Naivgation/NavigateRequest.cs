﻿using System;

namespace NoesisExtensions.Navigation
{
	public class NavigateRequest : INavigateRequest
	{
		Action<string, object[]> _request;

		Action<string, object[], Action<object[]>> _requestModal;

		public NavigateRequest(Action<string, object[]> request, Action<string, object[], Action<object[]>> requestModal)
		{
			_request = request;
			_requestModal = requestModal;
		}

		public void Navigate(string viewName, object[] parameters)
		{
			_request(viewName, parameters);
		}

        public void NavigateModal(string viewName, object[] parameters, Action<object[]> completed)
		{
			_requestModal(viewName, parameters, completed);
		}
	}
}
