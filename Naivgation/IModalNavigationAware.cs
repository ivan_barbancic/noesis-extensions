﻿using System;

namespace NoesisExtensions.Navigation
{
	public interface IModalNavigationAware : INavigationAware
    {      
		void NavigateToModal(Action<object[]> onComplete, params object[] parameters);
    }
}