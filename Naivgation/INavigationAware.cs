﻿using System;

namespace NoesisExtensions.Navigation
{
    public interface INavigationAware
	{      
        void NavigateFrom( Action navigationEnded );

        void RequestNavigateFrom( Action navigateFrom );

		void NavigateTo( INavigateRequest navigateRequest, params object[] parameters);      
    } 
}