﻿using Noesis;

namespace NoesisExtensions.Navigation
{   
	public interface IViewResolver
	{
		T Resolve<T>() where T : UserControl;
	}
}
