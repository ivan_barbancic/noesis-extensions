﻿using System;

namespace NoesisExtensions.Navigation
{
    public interface INavigateRequest
    {
		void Navigate(string viewName, object[] parameters);
          
        void NavigateModal(string viewName, object[] parameters, Action<object[]> completed);
	}
}
