﻿using System;
using Noesis;

namespace NoesisExtensions.Converters
{
	public class BoolToVisibilityConverter: IValueConverter
	{      
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{         
			bool? b = value as bool?;

			if ( b == true )
				return Visibility.Visible;
			else 
				return Visibility.Collapsed;         
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{         
			Visibility visibility = (Visibility)value;

			if ( visibility == Visibility.Visible )
				return true;
			else 
				return false;         
		}
	}   
}
