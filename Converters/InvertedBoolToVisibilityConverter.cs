﻿using System;
using Noesis;

namespace NoesisExtensions.Converters{

	public class InvertedBoolToVisibilityConverter: IValueConverter
	{      
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{         
			bool b = System.Convert.ToBoolean (value);

			if ( b == false )
				return Visibility.Visible;
			else 
				return Visibility.Collapsed;         
		}
			
		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{         
			Visibility visibility = (Visibility)value;

			if ( visibility == Visibility.Collapsed )
				return true;
			else 
				return false;         
		}
	}

}
